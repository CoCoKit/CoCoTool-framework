Pod::Spec.new do |s|
  s.name = "CoCoTool"
  s.version = "1.0.0.45"
  s.summary = "CoCoTool 用来处理CoCoUI中的一些东西."
  s.license = "MIT"
  s.requires_arc = true
  s.authors = {"iScarlett"=>"iScarlett@qq.com"}
  s.homepage = "https://gitlab.com/CoCoKit"
  s.description = "CoCoTool由自动化打包脚本生成提交,由最新push持续集成,保持最新" 
  s.source = { :git => 'https://gitlab.com/CoCoKit/CoCoTool-framework.git' }
  s.ios.deployment_target = '9.0'
  s.ios.vendored_framework = 'CoCoTool.framework'
  s.resource = "Resources/*.bundle"
  s.dependency 'Base64nl' ,'~> 1.2'
  s.dependency 'CoCoRNCryptor' ,'>= 0'
end
