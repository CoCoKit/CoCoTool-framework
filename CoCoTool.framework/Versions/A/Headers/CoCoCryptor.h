//
//  CoCoCryptor.h
//  CoCoTool
//
//  Created by 陈明 on 2017/11/9.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoCoCryptor : NSObject
+ (NSData *)aes256EncryptData:(NSData *)data error:(NSError **)error;
+ (NSData *)aes256DecryptData:(NSData *)data error:(NSError **)error;


+ (NSData *)aes256EncryptData:(NSData *)data pwd:(NSString *)pwd error:(NSError **)error;
+ (NSData *)aes256DecryptData:(NSData *)data pwd:(NSString *)pwd error:(NSError **)error;

+ (NSString *)aes256EncryptString:(NSString *)str pwd:(NSString *)pwd error:(NSError **)error;
+ (NSString *)aes256DecryptString:(NSString *)str pwd:(NSString *)pwd error:(NSError **)error;
@end
