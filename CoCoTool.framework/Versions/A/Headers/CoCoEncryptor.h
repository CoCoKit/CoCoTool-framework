//
//  CoCoEncryptor.h
//  WeiboCoCo
//
//  Created by 陈明 on 2016/11/1.
//  Copyright © 2016年 com.weibococo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoCoEncryptor : NSObject
+ (CoCoEncryptor *)sharedInstance;
- (NSString *)rsaEncryptor:(NSString *)text;
@end
