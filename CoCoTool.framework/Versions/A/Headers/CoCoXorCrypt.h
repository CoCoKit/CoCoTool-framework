//
//  CoCoXorCrypt.h
//  CoCoTool
//
//  Created by 陈明 on 2017/11/10.
//  Copyright © 2017年 CoCo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoCoXorCrypt : NSObject
+ (NSData *)EncryptData:(NSData *)data withKey:(NSString *)password;
+ (NSData *)DecryptData:(NSData *)data withKey:(NSString *)password;
@end
